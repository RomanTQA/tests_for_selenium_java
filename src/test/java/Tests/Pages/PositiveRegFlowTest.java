package Tests.Pages;

import Tests.CoreSettings.TestConfig;
import Tests.CoreSettings.Helpers;

import Tests.CoreSettings.TestData;
import Tests.CoreSettings.TestResultWatcher;
import Tests.Pages.Auth.LKPage;
import Tests.Pages.Auth.LoginPage;
import Tests.Pages.Auth.RegistrationPage;
import Tests.Pages.Auth.TempMailPage;
import Tests.Pages.BuyFlow.*;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertTrue;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.TestMethodOrder;
@ExtendWith(TestResultWatcher.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Tag("reg_and_auth")
@Feature("Allure Reporting")
@Owner("John Doe")
public class PositiveRegFlowTest extends TestConfig {


       /* @Test

        @DisplayName(" сегенрированный емейл реально копируется")
        @org.junit.jupiter.api.Order(1)
        public void fillRegForm()  {
            driver.get(RegistrationPage.getRegURL());
            RegistrationPage regPage = new RegistrationPage();
            regPage.goToTempMailTab()
                            .copyTempEmail()
                                    .switchBackToReg()
                                            .setTemporaryEmail();
            Helpers.waitForChange();


            Assertions.assertFalse((RegistrationPage.getRegEmail().getAttribute("value").contains("...")));

        }*/
    @Test
    @DisplayName("сравнение емейлов")
    @org.junit.jupiter.api.Order(1)
    public void CheckEmails()  {
        driver.get(RegistrationPage.getRegURL());
        RegistrationPage regPage = new RegistrationPage();
        regPage.goToTempMailTab();
        String tempMailVal;
        TempMailPage tmpMail = new TempMailPage();
        tmpMail.copyTempEmail();
        tempMailVal = TempMailPage.saveValueOFMail();
        tmpMail.switchBackToReg();
        Helpers.waitForChange();
        WebElement filledEmail = RegistrationPage.getRegEmail();
        filledEmail.sendKeys(tempMailVal);
        RegistrationPage.getFocusDropper().click();        //
        Helpers.waitForChange();
        Assertions.assertAll("value really copied",
                ()-> Assertions.assertFalse(filledEmail.getAttribute("value").contains("...")),
                ()-> Assertions.assertEquals(tempMailVal, (filledEmail.getAttribute("value"))));

    }
    @Test
    @DisplayName("Попытка зарегаться")
    @org.junit.jupiter.api.Order(2)
    public void regTest()  {
        driver.get(RegistrationPage.getRegURL());

        RegistrationPage regPage = new RegistrationPage();
        regPage.goToTempMailTab()
                        .copyTempEmail();
                     /*           .switchBackToReg()
                                        .setTemporaryEmail()
                                                .fillAllButEmail()
                                                        .DoRegistration();*/
        String tempMailVal =  TempMailPage.saveValueOFMail();
        TempMailPage tmpMail = new TempMailPage();
        tmpMail.switchBackToReg()
                .fillEmailFromString(tempMailVal)
                .fillAllButEmail()
                .DoRegistration();
        Helpers.waitForChange();

        Assertions.assertTrue(driver.getCurrentUrl().contains("personal"));


    }
    @Test
    @DisplayName("юзер регается, если оформил успешный заказ")
    @org.junit.jupiter.api.Order(3)
    public void regThroughBuyItem(){
        CatalogCard.makeOrderBasic();
        Helpers.waitForChange();
        WebElement nameContainer = OrderComplete.getNameContainer();
        Helpers.waitForChange();
        Assertions.assertTrue(nameContainer.isDisplayed());

    }

    @Test
    @DisplayName("проверка авторизации тестовым юзером")
    @org.junit.jupiter.api.Order(4)

    public void doTestAuth(){
        LoginPage auth = new LoginPage();
        auth.doAuthByTestUser();
        Helpers.waitForChange();
        Assertions.assertEquals(LKPage.getLkURL(), driver.getCurrentUrl());
    }
    @Test
    @DisplayName("Этот тест упадет")
    @Description("тест падает, для того чтобы отработал перехват коллбэка afterEach при упавшем тесте и сделался скрин")
    @Feature("is Screen available in the total report")
    @org.junit.jupiter.api.Order(5)

    public void doFailAuth(){
        LoginPage auth = new LoginPage();
        auth.failAuthByTestUser();
        Helpers.waitForChange();
        Assertions.assertEquals(LKPage.getLkURL(), driver.getCurrentUrl());
    }




}

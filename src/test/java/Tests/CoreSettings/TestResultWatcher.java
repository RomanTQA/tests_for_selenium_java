package Tests.CoreSettings;

import io.qameta.allure.Allure;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class TestResultWatcher implements AfterEachCallback {
    private static WebDriver driver;
    public static void setDriver(WebDriver webDriver) {
        driver = webDriver;
    }
    @Override
    public void afterEach(ExtensionContext context) {
        if (context.getExecutionException().isPresent()) {
            TakesScreenshot screenshotDriver = (TakesScreenshot) driver;
            byte[] screenshot = screenshotDriver.getScreenshotAs(OutputType.BYTES);
            String testName = context.getRequiredTestMethod().getName();
            String timestamp = String.valueOf(System.currentTimeMillis());
            String filename = testName + "_" + timestamp ;

            Allure.getLifecycle().addAttachment(filename, "image/png", "png", screenshot);

            TestConfig.deleteAllCookies(driver);
            TestConfig.closeCurrentWindow();
        }
    }


}
